# Kmail Enterprise header theme

Based on breeze style, optimized for laptop and desktop screens.

Optimized colors for all html emails (white);

## Install

Unzip and copy "KmailHeaderEnterprise" to `~/.local/share/messageviewer/themes/`

> *NOTE:* To keep clear html emails please disable color changing:
> Kmail → Konfigure → Appearance → Colors
> → enable "Do not change color from original HTML mail"

![Theme](imgs/theme.png)
